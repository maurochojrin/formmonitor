<?php

$config = require_once __DIR__.'/../config/config.php';

if (($domain = $_GET['domain']) && ($emailID = $_GET['id'])) {
    $sentEmailsPath = __DIR__ . "/../sites/$domain/sent_emails/";
    $fi = new FilesystemIterator($sentEmailsPath, FilesystemIterator::SKIP_DOTS);
    $logFileName = $config['open_log_file'];
    error_log("[$emailID] abierto el " . date('Y-m-d h:i:s') . PHP_EOL, 3, $logFileName);
    while ($fi->valid()) {
        $f = $fi->current();
        if (unlink($f)) {
            error_log("[$emailID] Eliminado el archivo testigo $f: " . date('Y-m-d h:i:s') . PHP_EOL, 3, $logFileName);
        } else {
            error_log("[$emailID] No se pudo eliminar el archivo testigo $f: " . date('Y-m-d h:i:s') . PHP_EOL, 3, $logFileName);
        }
        $fi->next();
    }
}
header('Content-Type: image/png;base64');
echo "wolQTkcNChoKAAAADUlIRFIAAAABAAAAAQgGAAAAHxXDhMKJAAAACklEQVR4AWMAAQAABQABNsOQwojDnQAAAABJRU5Ewq5CYMKC";