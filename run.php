#!/usr/bin/php

<?php

require_once 'vendor/autoload.php';
msg('Comienzo del proceso');
$config = require_once 'config/config.php';
msg('Configuracion general cargada del archivo ' . __DIR__ . '/config/config.php');

$opts = getopt('s:v', ['go']);
$go = array_key_exists('go', $opts);
$verbose = array_key_exists('v', $opts) || !$go;

if (array_key_exists('s', $opts)) {
    $targetSite = $opts['s'];
    msg('Procesando solo el sitio ' . $targetSite, $verbose);
    $siteConfigFileName = __DIR__ . '/sites/' . $targetSite . '/config.php';
    msg(' - Archivo de configuracion: ' . $siteConfigFileName, $verbose);
    $sites[$targetSite] = require_once $siteConfigFileName;
} else {
    msg('Escaneando el directorio ' . __DIR__ . '/sites/', $verbose);
    $sites = [];
    $fi = new FilesystemIterator(__DIR__ . '/sites/', FilesystemIterator::SKIP_DOTS);
    $today = new DateTime();
    while ($fi->valid()) {
        $domain = basename($fi->current());
        msg('== Evaluando configuracion del sitio ' . $domain, $verbose);
        $siteConfigFileName = $fi->current() . '/config.php';
        msg(' -- Archivo de configuracion: ' . $siteConfigFileName, $verbose);
        $siteConfig = require_once $siteConfigFileName;
        $baseDate = new DateTime($siteConfig['send']['base_date']);
        msg(' -- BaseDate: ' . $baseDate->format('d-m-Y'), $verbose);
        $daysInBetween = (int)($today->diff($baseDate)->format('%a'));
        msg(' -- DaysInBetween: ' . $daysInBetween, $verbose);
        $random = rand(0, 100);
        if (($daysInBetween % $siteConfig['send']['frecuency']) != 0) {
            msg(' -- No se enviara el dia de hoy debido a la frecuencia: ' . $siteConfig['send']['frecuency'], $verbose);
        } else if ($random > $siteConfig['send']['probability']) {
            msg(' -- No se enviara el dia de hoy debido a la probabilidad: ' . $siteConfig['send']['probability'], $verbose);
        } else {
            msg(' -- Encolado', $verbose);
            $sites[$domain] = $siteConfig;
        }
        msg('==', $verbose);
        $fi->next();
    }
}

use Goutte\Client;

$client = new Client();

msg('== Comenzando procesamiento de sitios', $verbose);

foreach ($sites as $domain => $siteConfig) {
    msg(' -- Procesando el sitio ' . $domain, $verbose);
    msg(' --- Descargando el form de ' . $siteConfig['form']['url'], $verbose);
    $crawler = $client->request(
        'GET',
        $siteConfig['form']['url']
    );

    $form = $crawler->filter(
        array_key_exists('filter', $siteConfig['form']) ? $siteConfig['form']['filter'] : 'form'
    )
        ->form();
    msg(' --- Form obtenido', $verbose);

    $validationEndPoint = str_replace('$domain', $domain, $config['validationEndPoint']);
    $emailId = uniqid();
    $validationEndPoint = str_replace('$id', $emailId, $validationEndPoint);

    msg(' --- Link a validacion ' . $validationEndPoint, $verbose);

    $array_rand = array_rand($siteConfig['form']['params']);
    $form_params = $siteConfig['form']['params'][$array_rand];
    $text = is_array( $siteConfig['text'] ) ? $siteConfig['text'][array_rand( $siteConfig['text'] )] : $siteConfig['text'];
    $form_params[$siteConfig['form']['text_field']] = $text . '<img src="' . $validationEndPoint . '" width="1" height="1"/>';
    msg(' --- Parametros del form: [ ' . str_replace( PHP_EOL, '', print_r( $form_params, 1 ) ). ' ]', $verbose);
    try {
        $form->setValues($form_params);
    } catch (InvalidArgumentException $e) {
        msg(' --- Fallo el llenado del form: ' . $e->getMessage().'. Dominio: '.$domain, true);
        continue;
    }

    msg(' --- Enviando POST a ' . $form->getUri(), $verbose);

    $sentEmailsPath = __DIR__ . "/sites/$domain/sent_emails/";
    $witnessfile = $sentEmailsPath . $emailId;
    if ($go) {
        $crawler = $client->submit($form);
        $response = $client->getResponse();

        if ($response->getStatus() == 200) {
            msg(' --- POST exitoso', $verbose);
            error_log("[" . date('Y-m-d h:i:s') . "] - [$domain] - $emailId" . PHP_EOL, 3, $config['send_log_file']);
            $contents = date('Y-m-d h:i:s').PHP_EOL;
            $contents .= ' --- Parametros del form: [ ' . str_replace( PHP_EOL, '', print_r( $form_params, 1 ) ). ' ]'.PHP_EOL;
            file_put_contents($witnessfile, $contents );
            chmod($witnessfile, 0666);
            msg(' --- Archivo testigo: ' . $witnessfile, $verbose);
        } else {
            msg( ' == $domain ==', true);
            msg(' --- Fallo el posteo. Error: ' . $response->getStatus(), true);
            msg(' --- Enviando alerta a ' . implode(',', $siteConfig['alert']['to']), true);
            $ch = curl_init($config['mailgun']['endPoint']);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:' . $config['mailgun']['api-key']);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                [
                    'from' => $siteConfig['alert']['from'],
                    'to' => implode(',', $siteConfig['alert']['to']),
                    'subject' => 'No se pudo llenar el form de contacto',
                    'text' => 'No fue posible completar el formulario de contacto de la pagina ' . $siteConfig['form']['url'].PHP_EOL.'Si la pagina cambio o se sabe la razon de este fallo, por favor, comuniquese con ' . $config['sysadmin'] . PHP_EOL.'Gracias.',
                ]
            );
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if ($info['http_code'] != 200) {
                msg(' --- No pudo enviarse la alerta: ' . $output, true);
            } else {
                msg(' --- Alerta enviada', $verbose);
            }
        }
    } else {
        msg(' -- Dry-run, no se envia nada', $verbose);
    }

    msg( ' == Procesamiento de archivos testigo', $verbose );

    $fi = new FilesystemIterator($sentEmailsPath, FilesystemIterator::SKIP_DOTS);
    $sentEmails = iterator_count($fi);
    msg(' --- Hay todavia ' . ($sentEmails - 1). ' esperando confirmacion.', $verbose);

    if ($sentEmails > $siteConfig['threshold']) {
        msg(' --- Se alcanzo el umbral, enviando alerta a ' . implode(',', $siteConfig['alert']['to']), $verbose);
        $killWitnesses = !$go;
        if ( $go ) {
            $ch = curl_init($config['mailgun']['endPoint']);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERPWD, 'api:' . $config['mailgun']['api-key']);
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                [
                    'from' => $siteConfig['alert']['from'],
                    'to' => implode(',', $siteConfig['alert']['to']),
                    'subject' => $siteConfig['alert']['subject'],
                    'text' => $siteConfig['alert']['text'],
                ]
            );
            $output = curl_exec($ch);
            $info = curl_getinfo($ch);
            curl_close($ch);
            if ($info['http_code'] == 200) {
                msg(' --- Alerta enviada.', $verbose);
                $killWitnesses = true;
            } else {
                msg(' --- No pudo enviarse la alerta: ' . $output, true);
            }
        }

        if ( $killWitnesses ) {
            msg( ' --- Eliminando archivos testigos', $verbose );
            $fi->rewind();
            while ($fi->valid()) {
                $f = $fi->current();
                if ( $f != $witnessfile ) {
                    if ( $go ) {
                        if (unlink($f)) {
                            msg(' --- Eliminado el archivo testigo ' . $f, $verbose );
                        } else {
                            msg(' --- No pudo eliminarse el archivo testigo ' . $f, true );
                        }
                    } else {
                        msg(' --- No se elimina el archivo testigo ' . $f .' por dry-run', $verbose );
                    }
                } else {
                    msg( ' --- Salteando el archivo testigo '.$f, $verbose );
                }
                $fi->next();
            }
        }
    }

    msg(' -- ', $verbose);
}

msg('Fin', $verbose);

/**
 * @param string $str
 * @param bool $verbose
 */
function msg( string $str, bool $verbose = false )
{
    if ( $verbose ) {
        echo $str . PHP_EOL;
    }
}