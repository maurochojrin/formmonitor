#!/usr/bin/php

<?php
/**
 * Created by PhpStorm.
 * User: mauro
 * Date: 3/14/18
 * Time: 10:25 AM
 */

$domain = $argv[1];

$domainDir = __DIR__ . '/sites/' . $domain;
if ( !mkdir($domainDir) ) {
    die( 'No se puede crear el directorio '.__DIR__.'/sites/'.$domain );
}

echo 'Directorio '.$domainDir.' creado'.PHP_EOL;

if ( !mkdir($domainDir.'/logs') ) {
    die( 'No se puede crear el directorio '.__DIR__.'/sites/'.$domain.'/logs' );
}

echo 'Directorio '.$domainDir.'/logs creado'.PHP_EOL;

if ( !mkdir($domainDir.'/sent_emails') ) {
    die( 'No se puede crear el directorio '.__DIR__.'/sites/'.$domain.'/sent_emails' );
}

echo 'Directorio '.$domainDir.'/sent_emails creado'.PHP_EOL;

$config = file_get_contents(__DIR__.'/site_config.tpl.php');
$config = str_replace('\'YYYY-MM-DD\'', "'".(new DateTime())->format('Y-m-d')."'", $config);
$config = str_replace('{$formUrl}', $argv[2], $config);
$config = str_replace('{$domain}', $domain, $config);
if ( array_key_exists(3, $argv ) ) {
    $config = str_replace('{$contacto}', $argv[3], $config);
}

if ( !file_put_contents($domainDir.'/config.php', $config)) {
    dir( 'No se puede copiar el archivo '.__DIR__.'/site_config.tpl.php'.' a '.$domainDir.'/config.php');
}

echo 'Archivo '.$domainDir.'/config.php creado'.PHP_EOL;



echo 'Todo listo! Ahora edite el archivo de configuracion para activar el sitio'.PHP_EOL;