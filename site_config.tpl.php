<?php
/**
 * This file contains specific configuration for a particular website to be tested.
 * A copy of it must exist as "config.php" inside a $domain directory within sites/
 * in order for the script run.php to process it.
 */

return [
    'text' => [
        'Esta es una prueba. No es necesario responder.<br/>Gracias.<br/>',
    ],
    'form' => [
        'url' => '{$formUrl}',
        'params' => [
            [
                'Name' => 'Maria',
                'email' => 'maria.pappen@gmail.com',
            ],
        ],
        'text_field' => 'message',
        //'filter' => 'form[name="contacto"]', // Filter to identify the form within the page
    ],
    'threshold' => 3,
    'alert' => [
        'text' => 'Los ultimos 3 correos no han sido abiertos.',
        'subject' => 'Hay un problema con el formulario de contacto de {$domain}. Contactar a {$contacto}.',
        'from' => 'Alertas de Leeway <mauro.chojrin@leewayweb.com>',
        'to' => [
            'mauro.chojrin@leewayweb.com',
        ],
    ],
    'send' => [
        'frecuency' => 5, // How many days between attempts
        'base_date' => 'YYYY-MM-DD', // The date to be used as first attempt ever
        'probability' => 100, // Number between 0 and 100 to determine the probability of a send being done
    ]
];