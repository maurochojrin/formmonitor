<?php

/**
 * This file contains general application configuration
 */

return [
    'validationEndPoint' => 'http://leewayweb.com:81/$domain/$id',
    'mailgun' =>
        [
            'endPoint' => 'https://api.mailgun.net/v3/mg.leewayweb.com/messages',
            'api-key' => 'key-a65a4b2e53a6df1e1a5b9b341723445f',
        ],
    'defaults' =>
        [
            'send' => [
                'frecuency' => 5,
                'probability' => 100,
                'base_date' => '2018-03-14',
            ],
        ],
    'send_log_file' => __DIR__.'/../logs/sends.log',
    'open_log_file' => __DIR__.'/../logs/opens.log',
    'sysadmin' => 'mauro.chojrin@leewayweb.com',
];
