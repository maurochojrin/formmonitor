#!/usr/bin/php

<?php
/**
 * Created by PhpStorm.
 * User: mauro
 * Date: 3/14/18
 * Time: 10:25 AM
 */

$domain = $argv[1];

$domainDir = __DIR__ . '/sites/' . $domain;
if ( !rename(__DIR__ . '/sites-disabled/' . $domain, $domainDir ) ) {
    die( 'No se puede moverse el directorio '.__DIR__ . '/sites-disabled/'.$domain.' a '.__DIR__.'/sites/'.$domain );
}

echo 'Dominio '.$domainDir.' habilitado. Para deshabilitarlo use disable-site.php'.PHP_EOL;