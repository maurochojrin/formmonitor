#!/usr/bin/php

<?php
/**
 * Created by PhpStorm.
 * User: mauro
 * Date: 3/14/18
 * Time: 10:25 AM
 */

$domain = $argv[1];

$domainDir = __DIR__ . '/sites/' . $domain;
if ( !rename($domainDir, __DIR__ . '/sites-disabled/' . $domain ) ) {
    die( 'No se puede moverse el directorio '.__DIR__.'/sites/'.$domain.' a '.__DIR__ . '/sites-disabled/' . $domain );
}

echo 'Dominio '.$domainDir.' deshabilitado. Para re-habilitarlo use enable-site.php'.PHP_EOL;